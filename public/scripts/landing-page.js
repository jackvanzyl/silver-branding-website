$(document).ready(function() {

  // ==================================================
  // Image Carousel
  // --------------------------------------------------

  $($('.carousel-image')[0]).addClass('active');

  $('#image-carousel-nav-left, #image-carousel-nav-right').click(function(event) {
    $('.carousel-image').each(function(i, e) {
      $(e).toggleClass('active');
    });
    event.preventDefault();
  });

  // --------------------------------------------------

});
